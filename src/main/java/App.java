import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class App {
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext(
                "Spring-Module.xml");

        HelloWorld obj = (HelloWorld) context.getBean("helloBean");
        obj.printHello();
        Greetings greets = (Greetings) context.getBean("helloGreetings");
        greets.printGreetings();
        /* public void setGreetings(String greetings) {
        this.Greetings=greetings;
    }*/
    }
}