public class Greetings {
    private String greetings;

    public void setGreetings(String greetings) {
        this.greetings = greetings;
    }

    public String printGreetings() {
        greetings = "Greetings " + greetings;
        return greetings;
    }
}