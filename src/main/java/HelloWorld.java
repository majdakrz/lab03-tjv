public class HelloWorld {
    private String name;
    private Greetings g;
    public void setName(String name) {
        this.name = name;
    }
    public void setG(Greetings g){
        this.g = g;
    }


    public void printHello() {
        System.out.println("Hello ! " + name + "\n" + g.printGreetings());
    }


}